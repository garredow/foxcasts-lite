# [2.2.0](https://github.com/garredow/foxcasts-lite/compare/v2.1.2...v2.2.0) (2021-11-30)


### Features

* back key now closes app menu ([a267a97](https://github.com/garredow/foxcasts-lite/commit/a267a974cc4c3394cabbabbb9bd134bd117e16a8))
* **player:** change volume with up/down keys ([cb63db4](https://github.com/garredow/foxcasts-lite/commit/cb63db4c3c5e03f58779209dd9b7889088113dcc))

## [2.1.2](https://github.com/garredow/foxcasts-lite/compare/v2.1.1...v2.1.2) (2021-11-12)


### Bug Fixes

* **import:** fix styling and subscribe behavior ([8c32ad0](https://github.com/garredow/foxcasts-lite/commit/8c32ad0091f63d355049a23c3fed6b9008cfe9d8))

## [2.1.1](https://github.com/garredow/foxcasts-lite/compare/v2.1.0...v2.1.1) (2021-11-12)

### Features

- Add CI/CD and automated builds
